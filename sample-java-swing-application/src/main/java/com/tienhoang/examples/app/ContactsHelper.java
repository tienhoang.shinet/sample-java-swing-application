package com.tienhoang.examples.app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ContactsHelper {
	private static final ContactsHelper INSTANCE = new ContactsHelper();
	
	public static ContactsHelper getInstance() {
		return ContactsHelper.INSTANCE;
	}
	
	private ContactsHelper() {}
	
	public List<Contact> getContacts() throws SQLException {
		final List<Contact> contacts = new ArrayList<>();
		
		final String sql = "SELECT * FROM contacts ORDER BY id";
		Connection connection = DbHelper.getConnection();
		PreparedStatement pstm = connection.prepareStatement(sql);
		ResultSet rs = pstm.executeQuery();
		
		while (rs.next()) {
			final Contact contact = new Contact();
			contact.setId(rs.getLong("id"));
			contact.setName(rs.getString("name"));
			contact.setContacts(rs.getString("contacts"));
			contacts.add(contact);
		}
		
		return contacts;
	}
}
