package com.tienhoang.examples.app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Contact {
	
	private long id = -1;
	private String name;
	private String contacts;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getContacts() {
		return contacts;
	}
	
	public void setContacts(String contacts) {
		this.contacts = contacts;
	}
	
	public void delete() throws SQLException {
		if (id != -1) {
			final String sql = "DELETE FROM contacts WHERE id = ?";
			Connection connection = DbHelper.getConnection();
			PreparedStatement pstm = connection.prepareStatement(sql);
			pstm.setLong(1, id);
			pstm.execute();
			id = -1;
		}
	}
	
	public void save() throws SQLException {
		Connection connection = DbHelper.getConnection();
		if (id == -1) {
			final String sql = "INSERT INTO contacts (name, contacts) VALUES (?, ?)";
			PreparedStatement pstm = connection.prepareStatement(sql);
			pstm.setString(1, name);
			pstm.setString(2, contacts);
			pstm.execute();
			ResultSet rs = pstm.getGeneratedKeys();
			rs.next();
			id = rs.getLong(1);
		} else {
			final String sql = "UPDATE contacts SET name = ?, contacts = ? WHERE id = ?";
			PreparedStatement pstm = connection.prepareStatement(sql);
			pstm.setString(1, name);
			pstm.setString(2, contacts);
			pstm.setLong(3, id);
			pstm.execute();
		}
	}

	@Override
	public String toString() {
		final StringBuilder formatted = new StringBuilder();
		if (id == -1) {
			formatted.append("[No id] ");
		} else {
			formatted.append("[").append(id).append("] ");
		}
		
		if (name == null) {
			formatted.append("no name");
		} else {
			formatted.append(name);
		}
		
		return formatted.toString();
	}
	
	
}
