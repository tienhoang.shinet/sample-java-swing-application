package com.tienhoang.examples.app;

import java.sql.SQLException;

import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
	
	public static void main(final String[] args) throws SQLException {

		DbHelper.getInstance().init();
		DbHelper.getInstance().registerShutdownHook();
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Main.LOGGER.debug("Starting application");
				Application app = new Application();
				app.setTitle("Simple Java Database Swing Application");
				app.setSize(800, 600);
				app.setDefaultCloseOperation(Application.EXIT_ON_CLOSE);
				app.setVisible(true);
				/*
				app.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosing(WindowEvent e) {
						Main.LOGGER.debug("DONE");
						DbHelper.getInstance().close();
					}
				});*/
			}
		});
	}
}
