package com.tienhoang.examples.app;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.Test;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

public class ContactsHelperTest {
	
	@Before
	public void init() throws SQLException {
		DbHelper.getInstance().init();
		
		Connection connection = DbHelper.getConnection();
		Statement stmt = connection.createStatement();
		stmt.execute("TRUNCATE TABLE contacts");
		stmt.execute("ALTER TABLE contacts ALTER COLUMN id RESTART WITH 1");
	}
	
	@After
	public void close() {
		DbHelper.getInstance().close();
	}
	
	@Test
	public void testLoad() throws SQLException {
		List<Contact> contacts = ContactsHelper.getInstance().getContacts();
		Assert.assertNotNull(contacts);
		Assert.assertTrue(contacts.isEmpty());
		
		Connection conn = DbHelper.getConnection();
		Statement stmt = conn.createStatement();
		stmt.execute("INSERT INTO contacts (name, contacts) VALUES ('test1', 'test1@gmail.com')");
		stmt.execute("INSERT INTO contacts (name, contacts) VALUES ('test2', 'test2@gmail.com')");
		stmt.execute("INSERT INTO contacts (name, contacts) VALUES ('test3', 'test3@gmail.com')");
		
		contacts = ContactsHelper.getInstance().getContacts();
		Assert.assertNotNull(contacts);
		Assert.assertEquals(3, contacts.size());
		
		Contact contact = contacts.get(0);
		Assert.assertNotNull(contact);
		Assert.assertEquals(1, contact.getId());
		Assert.assertEquals("test1", contact.getName());
		Assert.assertEquals("test1@gmail.com", contact.getContacts());
	}
}
