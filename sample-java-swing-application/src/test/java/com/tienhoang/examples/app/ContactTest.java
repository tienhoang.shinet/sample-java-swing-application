package com.tienhoang.examples.app;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.Test;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

public class ContactTest {
	
	@Before
	public void init() throws SQLException {
		DbHelper.getInstance().init();
		
		Connection connection = DbHelper.getConnection();
		Statement stmt = connection.createStatement();
		stmt.execute("TRUNCATE TABLE contacts");
		stmt.execute("ALTER TABLE contacts ALTER COLUMN id RESTART WITH 1");
	}
	
	@After
	public void close() {
		DbHelper.getInstance().close();
	}
	
	@Test
	public void testSave() throws SQLException {
		Contact c = new Contact();
		c.setName("test");
		c.setContacts("test@gmail.com");
		c.save();
		
		Connection conn = DbHelper.getConnection();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM contacts");
		Assert.assertTrue("Count should return at least one row", rs.next());
		Assert.assertEquals(1L, rs.getLong(1));
		Assert.assertFalse("Count should not return more than one row", rs.next());
		
		rs = stmt.executeQuery("SELECT * FROM contacts");
		Assert.assertTrue("Select should return at least one row", rs.next());
		Assert.assertEquals("test", rs.getString("name"));
		Assert.assertEquals(1L, rs.getLong("id"));
		Assert.assertEquals("test@gmail.com", rs.getString("contacts"));
		Assert.assertFalse("Select should return at least one row", rs.next());
		
	}
	
	@Test
	public void testDelete() throws SQLException {
		Connection conn = DbHelper.getConnection();
		Statement stmt = conn.createStatement();

		stmt.execute("INSERT INTO contacts (name, contacts) VALUES ('test1', 'test1@gmail.com')");
		stmt.execute("INSERT INTO contacts (name, contacts) VALUES ('test2', 'test2@gmail.com')");
		stmt.execute("INSERT INTO contacts (name, contacts) VALUES ('test3', 'test3@gmail.com')");
		
		List<Contact> contacts = ContactsHelper.getInstance().getContacts();
		Assert.assertEquals(3, contacts.size());

		final Contact contact = contacts.get(1);
		Assert.assertNotEquals(-1, contact.getId());
		contact.delete();
		Assert.assertEquals(-1, contact.getId());
		
		contacts = ContactsHelper.getInstance().getContacts();
		Assert.assertEquals(2, contacts.size());
		Assert.assertEquals(1L, contacts.get(0).getId());
		Assert.assertEquals(3L, contacts.get(1).getId());
		
	}
}
